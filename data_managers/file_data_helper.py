
import os
import json
from typing import Dict, Any


class FileDataManager:
    def __init__(self, location: str):
        self._location = location

    def write(self, data: Dict[str, Any], name: str):
        if not os.path.exists(self._location):
            os.makedirs(self._location)
        output_file = os.path.join(self._location, name)
        f = open(output_file, "w")
        f.write(json.dumps(data, indent=3))
        f.close()

    def read(self, name: str) -> Dict[str, Any]:
        data_file = os.path.join(self._location, name)
        with open(data_file) as f:
            data = json.load(f)
        return data
