from itertools import chain
from typing import Any, Dict, List, Optional

import requests
import json
import os

from pydantic import BaseModel

import config
from selection.request import SelectionRequest
from selection.results import Results

import selection.player as pl


class Position(BaseModel):
    element: int
    position: int
    is_captain: bool
    is_vice_captain: bool


class TeamRequest(BaseModel):
    chip: Optional[Any] = None
    picks: List[Position] = []


class Transfer(BaseModel):
    element_in: int
    element_out: int
    purchase_price: int
    selling_price: int


class TransferRequest(BaseModel):
    chip: Optional[Any] = None
    entry: int = 8736178
    event: int
    transfers: List[Transfer] = []


class FPLClient:

    def __init__(self, email: str, password: str):
        self._email = email
        self._password = password
        self._session = requests.session()
        self._position_scores = {
            pl.Position.GK: 1,
            pl.Position.DF: 2,
            pl.Position.MF: 3,
            pl.Position.FW: 4,
        }

    def login(self):
        url = "https://users.premierleague.com/accounts/login/"
        payload = {
            'password': f'{self._password}',
            'login': f'{self._email}',
            'redirect_uri': 'https://fantasy.premierleague.com/a/login',
            'app': 'plfpl-web'
        }
        response = self._session.post(url, data=payload)
        print(response)

    def get_team_data(self) -> Dict[str, Any]:
        url = "https://fantasy.premierleague.com/api/my-team/8736178/"
        response = self._session.get(url)
        return json.loads(response.content)

    def get_fpl_data(self) -> Dict[str, Any]:
        url = "https://fantasy.premierleague.com/api/bootstrap-static/"
        response = self._session.get(url)
        return json.loads(response.content)

    def get_fixture_data(self) -> Dict[str, Any]:
        url = "https://fantasy.premierleague.com/api/fixtures/"
        response = self._session.get(url)
        return json.loads(response.content)

    def get_live_data(self, gameweek) -> Dict[str, Any]:
        url = f"https://fantasy.premierleague.com/api/event/{gameweek}/live/"
        response = self._session.get(url)
        return json.loads(response.content)

    def post_team(self, result: Results):
        position = 1
        request = TeamRequest.construct()
        team_players = result.team_players

        team_players.sort(key=lambda x: self._position_scores[x.position])
        bench_players = result.bench_players
        bench_players.sort(key=lambda x: self._position_scores[x.position])

        for player in chain(team_players, bench_players):
            request.picks.append(Position.construct(element=player.id,
                                                    position=position,
                                                    is_captain=(player == result.captain),
                                                    is_vice_captain=(player == result.vice_captain)))
            position += 1

        url = "https://fantasy.premierleague.com/api/my-team/8736178/"

        print(request.json())
        response = self._session.post(url, json=request.dict())
        print(response)

    def post_transfers(self, selection_request: SelectionRequest, result: Results):
        url = "https://fantasy.premierleague.com/api/transfers/"

        request = TransferRequest.construct()
        request.event = selection_request.current_gameweek + 1
        new_squad = [player for player in chain(result.team_players, result.bench_players)]
        old_squad = [player for player in selection_request.current_team]

        transfer_in = sorted([player for player in new_squad if player not in old_squad],
                             key=lambda x: self._position_scores[x.position])
        transfer_out = sorted([player for player in old_squad if player not in new_squad],
                              key=lambda x: self._position_scores[x.position])

        assert len(transfer_in) == len(transfer_out)

        diff = 0
        for player_in, player_out in zip(transfer_in, transfer_out):
            request.transfers.append(Transfer.construct(
                element_in=player_in.id,
                element_out=player_out.id,
                purchase_price=int(player_in.purchase_price*10),
                selling_price=int(player_out.selling_price*10)
            ))
            diff += player_in.purchase_price*10
            diff -= player_out.selling_price*10

            print(f"Out: {player_out} ({int(player_out.selling_price*10)}) In: {player_in} ({int(player_in.purchase_price*10)})")

        print(f"diff - {diff/10} bank {selection_request.funds}")
        print((selection_request.funds - diff/10.0))
        assert((selection_request.funds - diff/10.0) >= 0)
        if not transfer_in:
            return
        response = self._session.post(url, json=request.dict())
        print(response)
