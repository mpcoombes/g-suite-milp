import datetime
import logging

import azure.functions as func

import datetime

from data_processor.fpl_processor import FPLDataProcessor, FPLMyTeamProcessor
from fpl_client.fpl_client import FPLClient
from lp.solvers.or_tools_solver import ORSolver
from selection.problem_builder import ProblemBuilder
from selection.request import SelectionRequest
from selection.results import ResultsBuilder


def run():
    client = FPLClient("mc1206matt@gmail.com", "Dupuku38")
    client.login()

    fpl_data = client.get_fpl_data()
    fpl_processor = FPLDataProcessor(fpl_data)

    team_data = client.get_team_data()
    fpl_processor = FPLDataProcessor(fpl_data)

    players = [player for player in fpl_processor.players()]
    gameweek = fpl_processor.gameweek()
    
    team_processor = FPLMyTeamProcessor(team_data, players)
    current_players = [player for player in team_processor.current_players()]
    request = SelectionRequest(
        players,
        current_team=current_players,
        funds=team_processor.bank(),
        current_gameweek=gameweek.get("id")
    )

    request.constraints.transfer_limit = team_processor.transfer_limit()

    if request.constraints.transfer_limit >= request.constraints.squad_players:
        request.constraints.transfer_penalty = 0.0

    problem, mappings = ProblemBuilder.build(request)

    # Solver
    solver = ORSolver()
    #solver = XpressSolver()
    status, output_problem = solver.solve(problem)
    print(status)
    
    # Results
    results = ResultsBuilder.build(mappings)
    deadline = gameweek.get('deadline_time')

    deadline_t = datetime.datetime.strptime(deadline, '%Y-%m-%dT%H:%M:%SZ')
    print(deadline_t)
    now = datetime.datetime.now()
    diff = now - deadline_t
    if diff.days < 1:
        client.post_transfers(request, results)
        client.post_team(results)


def main(mytimer: func.TimerRequest) -> None:
    utc_timestamp = datetime.datetime.utcnow().replace(
        tzinfo=datetime.timezone.utc).isoformat()

    if mytimer.past_due:
        logging.info('The timer is past due!')


    run()

    logging.info('Python timer trigger function ran at %s', utc_timestamp)
