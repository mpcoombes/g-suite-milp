import json
import os
import datetime

from config import PROJECT_PATH
from data_managers.file_data_helper import FileDataManager
from data_processor.fpl_processor import FPLDataProcessor, FPLMyTeamProcessor
from fpl_client.fpl_client import FPLClient
from lp.solvers.or_tools_solver import ORSolver
from lp.solvers.xpress_solver import XpressSolver
from selection.problem_builder import ProblemBuilder
from selection.request import SelectionRequest
from selection.results import ResultsBuilder

import statistics

def run():

    data_path = os.path.join(PROJECT_PATH, 'data')
    fetch_data = False
    if fetch_data:
        client = FPLClient("mc1206matt@gmail.com", "Dupuku38")
        client.login()
        fpl_data = client.get_fpl_data()
        fpl_processor = FPLDataProcessor(fpl_data)
        gw = fpl_processor.gameweek().get('id')
        data_helper = FileDataManager(os.path.join(data_path, str(gw)))
        data_helper.write(fpl_data, 'fpl_data.json')

        team_data = client.get_team_data()
        data_helper.write(team_data, 'team_data.json')

    else:
        folders = os.listdir(data_path)
        folders = [folder for folder in folders if not folder.startswith('.')]
        folders.sort(key=lambda x: int(x), reverse=True)
        assert folders
        data_path = os.path.join(data_path, folders[0])
        data_helper = FileDataManager(data_path)
        fpl_data = data_helper.read('fpl_data.json')
        team_data = data_helper.read('team_data.json')

    fpl_processor = FPLDataProcessor(fpl_data)
    players = [player for player in fpl_processor.players()]
    var = 0
    diffs = []
    for player in fpl_data.get("elements"):
        ep_this = float(player.get("ep_this"))
        if ep_this == 0:
            continue
        points = player.get("event_points")
        diff = ep_this - points
        diffs.append(diff)

    print(statistics.mean(diffs))
    print(statistics.stdev(diffs))



if __name__ == '__main__':
    run()
