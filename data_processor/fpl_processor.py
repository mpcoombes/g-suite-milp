import math
from typing import Iterator, Dict, Any, List

from selection.player import Player, Position


class FPLDataProcessor:

    def __init__(self, data: Dict[str, Any]):
        self._data = data
        self._positions = {1: Position.GK,
                           2: Position.DF,
                           3: Position.MF,
                           4: Position.FW}

        self._teams = {team['code']: team['name'] for team in data.get("teams", [])}

    def gameweek(self) -> Dict[str, Any]:
        return next((event for event in self._data.get('events', []) if event.get('is_current')))

    def next_gameweek(self) -> Dict[str, Any]:
        return next((event for event in self._data.get('events', []) if event.get('is_next')))

    def players(self) -> Iterator[Player]:
        for element in self._data.get('elements', []):
            if not element['chance_of_playing_next_round']:
                continue
            player = Player(element['id'],
                            element['web_name'],
                            self._teams[element['team_code']],
                            self._positions[element['element_type']],
                            math.inf if not element['now_cost'] else float(element['now_cost']) / 10,
                            0 if not element['ep_next'] else float(element['ep_next']))
            yield player


class FPLMyTeamProcessor:

    def __init__(self, data: Dict[str, Any], players: List[Player]):
        self._data = data
        self._players = players

    def current_players(self) -> Iterator[Player]:
        for p in self._data["picks"]:
            id = p["element"]
            player = next(player for player in self._players if player.id == id)
            player.selling_price = p["selling_price"]/10.0
            yield player

    def bank(self) -> float:
        return self._data["transfers"]["bank"]/10.0

    def transfer_limit(self):
        if self._data["transfers"]["limit"]:
            return self._data["transfers"]["limit"] - self._data["transfers"]["made"]
        return 20

