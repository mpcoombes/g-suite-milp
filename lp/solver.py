from lp.problem.problem import Problem
from lp.solvers.or_tools_solver import ORSolver
from lp.solvers.status import Status


def solve(problem: Problem) -> Status:
    solver = ORSolver()
    status = solver.solve(problem)
    return status
