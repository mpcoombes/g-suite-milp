import json
import unittest

from lp.problem.problem import Problem


class TestProblemSerialisation(unittest.TestCase):
    def test_case_1(self):
        j = """
        {
    "variables": [
        {
            "id": 1,
            "name": "x",
            "is_discrete": false,
            "value": null,
            "minimum": 0.0,
            "maximum": 10.0
        },
        {
            "id": 2,
            "name": "y",
            "is_discrete": false,
            "value": null,
            "minimum": 0.0,
            "maximum": 10.0
        },
        {
            "id": 3,
            "name": "objective",
            "is_discrete": false,
            "value": null,
            "minimum": -Infinity,
            "maximum": Infinity
        }
    ],
    "equations": [
        {
            "id": 1,
            "name": "",
            "terms": [
                {
                    "coefficient": 1.0,
                    "variable_id": 1
                },
                {
                    "coefficient": 1.0,
                    "variable_id": 2
                },
                {
                    "coefficient": -1.0,
                    "variable_id": 3
                }
            ],
            "value": null,
            "lower_bound": 0.0,
            "upper_bound": 0.0
        }
    ],
    "objective_id": 3
}
        """
        problem = Problem(**json.loads(j))
        print(problem)

        self.assertIsInstance(problem, Problem)
        self.assertEqual(problem.objective().id, 3)


if __name__ == '__main__':
    unittest.main()
