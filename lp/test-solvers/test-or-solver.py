import unittest

from lp.problem.equation import Term, Equation
from lp.problem.problem import Problem
from lp.problem.variable import Variable
from lp.solvers.or_tools_solver import ORSolver
from lp.solvers.status import Status


class TestORSolver(unittest.TestCase):
    def test_case_1(self):
        # Setup
        x = Variable(id=1, name="x", minimum=0, maximum=10)
        y = Variable(id=2, name="y", minimum=0, maximum=10)
        objective = Variable(id=3, name="objective")
        eqn = Equation(id=1, lower_bound=0, upper_bound=0,
                       terms=[Term(variable_id=1),
                              Term(variable_id=2),
                              Term(coefficient=-1.0, variable_id=3)])

        problem = Problem(variables=[x, y, objective],
                          equations=[eqn],
                          objective_id=objective.id)

        solver = ORSolver()

        # Act
        status, output_problem = solver.solve(problem)

        # Asset
        self.assertEqual(Status.Optimal, status)

        self.assertEqual(10, x.value)
        self.assertEqual(10, y.value)


if __name__ == '__main__':
    unittest.main()
