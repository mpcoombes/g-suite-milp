import math
from dataclasses import dataclass, field
from typing import Iterable, List

from lp.problem.variable import Variable


@dataclass
class Term:
    variables: List[Variable]
    coefficient: float = 1.0
    constant: float = 0.0

    def is_linear(self):
        return len(self.variables) < 2


@dataclass
class Equation:
    id: int
    name: str = f""
    lower_bound: float = -math.inf
    upper_bound: float = math.inf
    terms: List[Term] = field(default_factory=list)
    value: float = None

    def __hash__(self) -> int:
        return self.id

    def __eq__(self, other) -> bool:
        return isinstance(other, Equation) and other.id == self.id
