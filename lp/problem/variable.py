import math
from dataclasses import dataclass


@dataclass
class Variable:
    id: int
    name: str = f""
    minimum: float = -math.inf
    maximum: float = math.inf
    is_discrete: bool = False
    value: float = None

    def __hash__(self) -> int:
        return self.id

    def __eq__(self, other) -> bool:
        return isinstance(other, Variable) and other.id == self.id
