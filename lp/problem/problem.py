from dataclasses import dataclass, field
from typing import List

from lp.problem.equation import Equation
from lp.problem.variable import Variable

@dataclass
class Problem:
    variables: List[Variable] = field(default_factory=list)
    equations: List[Equation] = field(default_factory=list)
    objective_id: int = 0

    def objective(self):
        return next(var for var in self.variables if var.id == self.objective_id)

