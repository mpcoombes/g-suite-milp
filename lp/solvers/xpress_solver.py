

# noinspection PyUnresolvedReferences
import math
import warnings
from typing import Dict, Any, List, Iterator

from lp.problem.equation import Equation
from lp.solvers.status import Status

with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    import xpress as xp

from lp.problem.problem import Problem
from lp.problem.variable import Variable


def _init_licence():
    try:
        oem_number = 99878378
        if xp.beginlicensing():
            licence_message = ""
            licence_value, licence_message = xp.license(0, licence_message)
            licence_value = oem_number - licence_value * licence_value // 19
            xp.license(licence_value, licence_message)
            xp.init()
            xp.endlicensing()
    except:
        pass


class XpressSolver:
    _init_licence()

    def __init__(self):
        self._lp = xp.problem(name="Problem")
        # self._lp.setControl('xslp_mipalgorithm', 1047)
        # self._lp.setControl('xslp_mipiterlimit', 2)
        # self._lp.setControl('xslp_iterlimit', 2)
        # self._lp.setControl('xslp_iterlimit', 2)
        # self._lp.setControl('xslp_iterlimit', 2)

                # print(self._lp.controls)
        # for c, v in self._lp.controls.items():
        #     print(f"{c}: {v}\n")
      #  self._lp.setControl('miprelstop', 1e2)
      #  self._lp.setControl({'miprelstop': 1e2, 'feastol': 1e-3})
        self.problem_var_to_solver_var: Dict[Variable, Any] = {}

    def solve(self, problem: Problem):
        self._add_variables(problem.variables)
        self._add_equations(problem.variables, problem.equations)
        self._set_objective(problem)

        self._lp.solve()
        self._lp.postsolve()
        status = self._lp.getProbStatus()

        if self._is_optimal(status):
            self._set_values()
            return Status.Optimal, problem
        return Status.Unknown, problem

    def _set_objective(self, problem: Problem):
        self._lp.setObjective(self.problem_var_to_solver_var[problem.objective()], xp.maximize)

    def _add_variables(self, variables: Iterator[Variable]):
        for var in variables:
            lb = self._convert(var.minimum)
            ub = self._convert(var.maximum)
            xp_var = xp.var(lb=lb,
                            ub=ub,
                            vartype=xp.binary if var.is_discrete else xp.continuous)
            self.problem_var_to_solver_var[var] = xp_var
            self._lp.addVariable(xp_var)

    def _add_equations(self, variables: List[Variable], equations: List[Equation]):
        for eqn in equations:
            body = xp.Sum(xp.Prod([self.problem_var_to_solver_var[var] for var in term.variables]) * term.coefficient + term.constant for term in eqn.terms)
            if eqn.lower_bound is not None and eqn.lower_bound != -math.inf:

                xp_eqn = xp.constraint(
                    body=xp.Sum(xp.Prod([self.problem_var_to_solver_var[var] for var in term.variables]) * term.coefficient + term.constant for term in eqn.terms),
                    sense=xp.geq,
                    rhs=self._convert(eqn.lower_bound))
                self._lp.addConstraint(xp_eqn)

            if eqn.upper_bound is not None and eqn.upper_bound != math.inf:
                xp_eqn = xp.constraint(
                    body=xp.Sum(xp.Prod([self.problem_var_to_solver_var[var] for var in term.variables]) * term.coefficient + term.constant for term in eqn.terms),
                    sense=xp.leq,
                    rhs=self._convert(eqn.upper_bound))
                self._lp.addConstraint(xp_eqn)

    def _set_values(self):
        solutions = self._lp.getSolution([var for var in self.problem_var_to_solver_var.values()], flatten=True)
        for var, sol in zip(self.problem_var_to_solver_var.keys(), solutions):
            var.value = sol

    def _convert(self, value: float):
        if not math.isinf(value):
            return value
        elif value > 0.0:
            return +xp.infinity
        else:
            return -xp.infinity

    def _is_optimal(self, xp_status: int) -> bool:
        return xp_status == xp.mip_optimal \
               or xp_status == xp.lp_optimal

    @staticmethod
    def _convert_to_xp_inf(value: float):
        if not math.isinf(value):
            return value
        elif value > 0.0:
            return +xp.infinity
        else:
            return -xp.infinity
