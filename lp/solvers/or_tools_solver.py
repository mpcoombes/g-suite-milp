import math
from typing import Iterator, Any, Dict, List

from ortools.linear_solver import pywraplp

from lp.problem.equation import Equation
from lp.problem.problem import Problem
from lp.problem.variable import Variable
from lp.solvers.status import Status


class ORSolver:
    def __init__(self):
        self._solver = pywraplp.Solver.CreateSolver('SCIP')
        self._solver.EnableOutput()
        self.problem_var_to_solver_var: Dict[Variable, Any] = {}

    def solve(self, problem: Problem):
        self._add_variables(problem.variables)
        self._add_equations(problem.variables, problem.equations)
        self._set_objective(problem)

        status = self._solver.Solve()
        if status == pywraplp.Solver.OPTIMAL:
            self._set_values()
            return Status.Optimal, problem
        return Status.Unknown, problem

    def _set_objective(self, problem: Problem):
        objective = self._solver.Objective()
        objective.SetMaximization()
        obj_var = self.problem_var_to_solver_var[problem.objective()]
        objective.SetCoefficient(obj_var, 1.0)

    def _add_variables(self, variables: Iterator[Variable]):
        for var in variables:
            v = self._solver.Var(
                self._convert(var.minimum),
                self._convert(var.maximum),
                var.is_discrete,
                var.name)
            self.problem_var_to_solver_var[var] = v

    def _add_equations(self, variables: List[Variable], equations: List[Equation]):
        for eqn in equations:
            constraint = self._solver.Constraint()
            constant = 0
            for term in eqn.terms:
                assert term.is_linear()
                if term.variables:
                    s_var = self.problem_var_to_solver_var[term.variables[0]]
                    constraint.SetCoefficient(s_var, term.coefficient)
                constant += term.constant
            if eqn.lower_bound is not None and eqn.lower_bound != -math.inf:
                constraint.SetLb(self._convert(eqn.lower_bound) - constant)
            if eqn.upper_bound is not None and eqn.upper_bound != math.inf:
                constraint.SetUb(self._convert(eqn.upper_bound) - constant)

    def _set_values(self):
        for var, s_var in self.problem_var_to_solver_var.items():
            var.value = s_var.solution_value()

    def _convert(self, number: float):
        if number == math.inf:
            return self._solver.infinity()
        if number == -math.inf:
            return -self._solver.infinity()
        return number
