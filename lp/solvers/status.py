from enum import Enum


class Status(Enum):
    Optimal = 1
    Unknown = 2
