import json
import os
import datetime

from config import PROJECT_PATH
from data_managers.file_data_helper import FileDataManager
from data_processor.fpl_processor import FPLDataProcessor, FPLMyTeamProcessor
from fpl_client.fpl_client import FPLClient
from lp.solvers.or_tools_solver import ORSolver
from lp.solvers.xpress_solver import XpressSolver
from selection.problem_builder import ProblemBuilder
from selection.request import SelectionRequest
from selection.results import ResultsBuilder


def run():

    client = FPLClient("mc1206matt@gmail.com", "Dupuku38")
    client.login()

    data_path = os.path.join(PROJECT_PATH, 'data')
    fetch_data = True
    if fetch_data:
        fpl_data = client.get_fpl_data()
        fpl_processor = FPLDataProcessor(fpl_data)
        gw = fpl_processor.gameweek().get('id')
        data_helper = FileDataManager(os.path.join(data_path, str(gw)))
        data_helper.write(fpl_data, 'fpl_data.json')

        team_data = client.get_team_data()
        data_helper.write(team_data, 'team_data.json')

        fixture_data = client.get_fixture_data()
        data_helper.write(fixture_data, 'fixtures.json')

        live_data = client.get_live_data(gw)
        data_helper.write(live_data, 'live_data.json')
    else:
        folders = os.listdir(data_path)
        folders = [folder for folder in folders if not folder.startswith('.')]
        folders.sort(key=lambda x: int(x), reverse=True)
        assert folders
        data_path = os.path.join(data_path, folders[0])
        data_helper = FileDataManager(data_path)
        fpl_data = data_helper.read('fpl_data.json')
        team_data = data_helper.read('team_data.json')

    fpl_processor = FPLDataProcessor(fpl_data)
    players = [player for player in fpl_processor.players()]
    gameweek = fpl_processor.gameweek()
    team_processor = FPLMyTeamProcessor(team_data, players)
    current_players = [player for player in team_processor.current_players()]
    request = SelectionRequest(
        players,
        current_team=current_players,
        funds=team_processor.bank(),
        current_gameweek=gameweek.get("id")
    )

    next_gameweek = fpl_processor.next_gameweek()

    request.constraints.transfer_limit = team_processor.transfer_limit()
    request.constraints.transfer_penalty = (2.0 - request.constraints.transfer_limit) * 2.0
    #print(f"transfer limit: {request.constraints.transfer_limit}")
    if request.constraints.transfer_limit >= request.constraints.squad_players:
        request.constraints.transfer_penalty = 0.0

    problem, mappings = ProblemBuilder.build(request)

    # Solver
    solver = ORSolver() #careful the MIP here is not working as expected!!
    #solver = XpressSolver()
    status, output_problem = solver.solve(problem)

    # Results
    print(status)
    #print(f"sales {mappings.sales.value}")
    results = ResultsBuilder.build(mappings)
    deadline = next_gameweek.get('deadline_time')

    deadline_t = datetime.datetime.strptime(deadline, '%Y-%m-%dT%H:%M:%SZ')
    print(deadline_t)
    now = datetime.datetime.now()
    diff = deadline_t - now
    if diff.days < 1:
        # Do transfers automatically
        # Update the teams
        print("test")

    #client.post_transfers(request, results)
    #client.post_team(results)

    print(diff)
    print(next_gameweek)

    print(results.pretty_print())
    #print(mappings.num_transfers_var.value)


if __name__ == '__main__':
    run()
