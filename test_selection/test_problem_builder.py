import unittest

from lp.solvers.or_tools_solver import ORSolver
from lp.solvers.status import Status
from lp.solvers.xpress_solver import XpressSolver
from selection.player import Position, Player
from selection.problem_builder import ProblemBuilder
from selection.request import SelectionConstraints, Limit, SelectionRequest

#
# class TestProblemBuilder(unittest.TestCase):
#     def test_problem_builder(self):
#         self.assertEqual(True, False)  # add assertion here
#
from selection.results import ResultsBuilder


class TestProblemBuilderIntegration(unittest.TestCase):
    def setUp(self) -> None:
        self.constraints = SelectionConstraints()
        self.constraints.squad_players = 4
        self.constraints.team_players = 3
        self.constraints.max_players_per_team = 2
        self.constraints.squad_positions = dict()
        self.constraints.squad_positions[Position.DF] = 2
        self.constraints.squad_positions[Position.FW] = 2
        self.constraints.team_positions = dict()
        self.constraints.team_positions[Position.DF] = Limit(1, 3)
        self.constraints.team_positions[Position.FW] = Limit(1, 3)

    def test_problem_builder(self):
        players = list()
        players.append(Player(1, f"Player {1}", "Team1", Position.DF, 10.0, 20.0))
        players.append(Player(2, f"Player {2}", "Team1", Position.DF, 80.0, 100.0))
        players.append(Player(3, f"Player {3}", "Team2", Position.DF, 10.0, 10.0))
        players.append(Player(4, f"Player {4}", "Team3", Position.FW, 10.0, 5.0))
        players.append(Player(5, f"Player {5}", "Team4", Position.FW, 10.0, 60.0))
        players.append(Player(6, f"Player {6}", "Team2", Position.FW, 80.0, 100.0))

        selection = SelectionRequest(players, self.constraints)

        problem, mappings = ProblemBuilder.build(selection)

        # Solver
        solver = XpressSolver()

        # Act
        status, output_problem = solver.solve(problem)

        # Asset
        self.assertEqual(Status.Optimal, status)

        # Results
        results = ResultsBuilder.build(mappings)

        self.assertEqual(3, len(results.team_players))
        self.assertIn(players[2], results.team_players)
        self.assertIn(players[3], results.team_players)
        self.assertIn(players[4], results.team_players)

        self.assertEqual(1, len(results.bench_players))
        self.assertIn(players[0], results.bench_players)

        print(results.captain)
        self.assertIn(players[0], results.captain)
        print(results)


if __name__ == '__main__':
    unittest.main()
