from dataclasses import dataclass


class Position:
    GK = "GK"
    DF = "DF"
    MF = "MF"
    FW = "FW"


@dataclass
class Player:
    id: int
    name: str
    team: str
    position: str
    purchase_price: float
    points: float = 0
    selling_price: float = 0

    def __hash__(self):
        return self.id

    def __eq__(self, other):
        return other.id == self.id

    def __repr__(self) -> str:
        return f"{self.name}({self.id})"
