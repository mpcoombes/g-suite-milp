import math
from dataclasses import dataclass, field
from itertools import count
from typing import Dict, Tuple, Optional

from lp.problem.equation import Equation, Term
from lp.problem.problem import Problem
from lp.problem.variable import Variable
from selection.player import Player, Position
from selection.request import SelectionRequest


BIG_M = 1.0e6


@dataclass
class ProblemMappings:
    player_squad_var: Dict[Player, Variable] = field(default_factory=dict)
    player_team_var: Dict[Player, Variable] = field(default_factory=dict)
    player_bench_var: Dict[Player, Variable] = field(default_factory=dict)
    player_captain_var: Dict[Player, Variable] = field(default_factory=dict)
    player_sell_var: Dict[Player, Variable] = field(default_factory=dict)
    num_transfers_var: Optional[Variable] = None
    sales: Optional[Variable] = None
    player_purchase_var: Dict[Player, Variable] = field(default_factory=dict)
    player_bench_pos_var: Dict[Tuple[int, Player], Variable] = field(default_factory=dict)


class ProblemBuilder:
    @staticmethod
    def build(request: SelectionRequest) -> Tuple[Problem, ProblemMappings]:
        builder = _ProblemBuilderImpl(request).build()
        return builder.problem, builder.mappings


class _ProblemBuilderImpl:
    def __init__(self, request: SelectionRequest):
        self._request = request
        self._problem = Problem()
        self.id_gen = count(0)
        self._mappings = ProblemMappings()
        self._transfer_slack_var = None

    @property
    def problem(self) -> Problem:
        return self._problem

    @property
    def mappings(self) -> ProblemMappings:
        return self._mappings

    def build(self):
        self._build_player_variables()
        self._build_equations()
        return self

    def _build_player_variables(self):
        self._build_team_players()
        self._build_bench_players()
        self._build_squad_players()
        self._build_captain_players()
        self._build_bench_pos_players()
        self._build_player_tie_equations()

    def _build_squad_players(self):
        for player in self._request.players:
            var = Variable(next(self.id_gen), f"{player.name} Squad", 0, 1, is_discrete=True)
            self._problem.variables.append(var)
            self._mappings.player_squad_var[player] = var

    def _build_team_players(self):
        for player in self._request.players:
            var = Variable(next(self.id_gen), f"{player.name} Team", 0, 1, is_discrete=True)
            self._problem.variables.append(var)
            self._mappings.player_team_var[player] = var

    def _build_bench_players(self):
        for player in self._request.players:
            var = Variable(next(self.id_gen), f"{player.name} Bench", 0, 1, is_discrete=True)
            self._problem.variables.append(var)
            self._mappings.player_bench_var[player] = var

    def _build_bench_pos_players(self):
        for i in range(0, 4):
            for player in self._request.players:
                var = Variable(next(self.id_gen), f"{player.name} Bench {i + 1}", 0, 1, is_discrete=True)
                self._problem.variables.append(var)
                self._mappings.player_bench_pos_var[(i, player)] = var

    def _build_bench_pos_structure(self):
        for i in range(0, 4):
            total_eqn = Equation(next(self.id_gen), f"total bench pos {i + 1}", 1, 1)
            self._problem.equations.append(total_eqn)
            for player in self._request.players:
                var = self._mappings.player_bench_pos_var[(i, player)]
                total_eqn.terms.append(Term([var]))

                eqn = Equation(next(self.id_gen), f"bench {i} {player} must be in bench max", upper_bound=1)
                eqn.terms = [Term([self._mappings.player_bench_var[player]]),
                             Term([var], +BIG_M),
                             Term([], constant=-BIG_M)]
                self._problem.equations.append(eqn)

                eqn = Equation(next(self.id_gen), f"bench {i} {player} must be in bench min", lower_bound=1)
                eqn.terms = [Term([self._mappings.player_bench_var[player]]),
                             Term([var], -BIG_M),
                             Term([], constant=BIG_M)]
                self._problem.equations.append(eqn)

    def _build_captain_players(self):
        for player in self._request.players:
            var = Variable(next(self.id_gen), f"{player.name} Captain", 0, 1, is_discrete=True)
            self._problem.variables.append(var)
            self._mappings.player_captain_var[player] = var

    def _build_captain_equations(self):
        captain_eqn = Equation(next(self.id_gen), f"one captain", 0, 1)
        captain_eqn.terms = [Term([self._mappings.player_captain_var[player]])
                             for player in self._request.players]
        self._problem.equations.append(captain_eqn)

        for player, var in self._mappings.player_captain_var.items():
            eqn = Equation(next(self.id_gen), f"captain {player} must be in team max", upper_bound=1)
            eqn.terms = [Term([self._mappings.player_team_var[player]]),
                         Term([var], +BIG_M),
                         Term([], constant=-BIG_M)]
            self._problem.equations.append(eqn)

            eqn = Equation(next(self.id_gen), f"captain {player} must be in team min", lower_bound=1)
            eqn.terms = [Term([self._mappings.player_team_var[player]]),
                         Term([var], -BIG_M),
                         Term([], constant=BIG_M)]
            self._problem.equations.append(eqn)

    def _build_player_tie_equations(self):
        for player, var in self._mappings.player_squad_var.items():
            eqn = Equation(next(self.id_gen), f"{player.name} Squad Limit", 0, 0)
            eqn.terms.append(Term([self._mappings.player_squad_var[player]], -1.0))
            eqn.terms.append(Term([self._mappings.player_team_var[player]], 1.0))
            eqn.terms.append(Term([self._mappings.player_bench_var[player]], 1.0))
            self._problem.equations.append(eqn)

    def _build_equations(self):
        self._build_bench_pos_structure()
        self._build_position_limits()
        self._build_team_limits()
        self._build_player_limits()
        self._build_captain_equations()
        self._build_transfers_structure()
        self._build_cost()
        self._build_objective()

    def _build_position_limits(self):
        for position in [Position.GK, Position.DF, Position.MF, Position.FW]:
            players = [player for player in self._request.players if player.position == position]
            if not players:
                continue
            eqn = Equation(next(self.id_gen), f"{position} Squad Limit",
                           self._request.constraints.squad_positions[position],
                           self._request.constraints.squad_positions[position])
            eqn.terms = [Term([self._mappings.player_squad_var[player]]) for player in players]
            self._problem.equations.append(eqn)

            eqn = Equation(next(self.id_gen), f"{position} Team Limit",
                           self._request.constraints.team_positions[position].minimum,
                           self._request.constraints.team_positions[position].maximum)
            eqn.terms = [Term([self._mappings.player_team_var[player]]) for player in players]
            self._problem.equations.append(eqn)

    def _build_team_limits(self):
        teams = {player.team for player in self._request.players}
        for team in teams:
            players = [player for player in self._request.players if player.team == team]
            eqn = Equation(next(self.id_gen), f"{team} Team Limit",
                           0,
                           self._request.constraints.max_players_per_team)
            eqn.terms = [Term([self._mappings.player_squad_var[player]]) for player in players]
            self._problem.equations.append(eqn)

    def _build_cost(self):
        cost_eqn = Equation(next(self.id_gen), f"cost", 0, self._request.funds)
        for player, var in self._mappings.player_squad_var.items():
            if player not in self._request.current_team:
                term1 = Term([var], player.purchase_price)
                cost_eqn.terms.append(term1)
                var = Variable(next(self.id_gen), f"player {player} purchase", is_discrete=False)
                self._problem.variables.append(var)
                eqn = Equation(next(self.id_gen), f"cost", 0, 0)
                eqn.terms.append(Term([var], -1))
                eqn.terms.append(term1)
                self._problem.equations.append(eqn)
                self._mappings.player_purchase_var[player] = var

        cost_eqn.terms.append(Term([self._mappings.sales], -1.0))
        self._problem.equations.append(cost_eqn)

    def _build_objective(self):
        obj_eqn = Equation(next(self.id_gen), "obj", 0, 0)

        obj_var = Variable(next(self.id_gen), "obj")
        self._problem.variables.append(obj_var)
        self._problem.objective_id = obj_var.id
        obj_eqn.terms.append(Term([obj_var], -1.0))

        obj_eqn.terms.extend(Term([var], player.points)
                             for player, var in self._mappings.player_team_var.items())
        obj_eqn.terms.extend(Term([var], self._request.constraints.bench_objective_coefficient * player.points)
                             for player, var in self._mappings.player_bench_var.items())

        # add double points for captain
        obj_eqn.terms.extend(Term([var], player.points)
                             for player, var in self._mappings.player_captain_var.items())

        # transfers
        obj_eqn.terms.append(Term([self._mappings.num_transfers_var], -1 * self._request.constraints.transfer_penalty))
        obj_eqn.terms.append(Term([self._transfer_slack_var], -1 * 5.0)) # add as penalty for more transfers

        self._problem.equations.append(obj_eqn)

    def _build_player_limits(self):
        team_eqn = Equation(next(self.id_gen), f"team",
                            self._request.constraints.team_players,
                            self._request.constraints.team_players)
        team_eqn.terms = [Term([self._mappings.player_team_var[player]])
                          for player in self._request.players]
        self._problem.equations.append(team_eqn)

        squad_eqn = Equation(next(self.id_gen), f"squad",
                             self._request.constraints.squad_players,
                             self._request.constraints.squad_players)
        squad_eqn.terms = [Term([self._mappings.player_squad_var[player]])
                           for player in self._request.players]
        self._problem.equations.append(squad_eqn)

    def _build_transfers_structure(self):
        num_free_transfers = self._request.constraints.transfer_limit
        var_transfers = Variable(next(self.id_gen), "transfers", minimum=0, maximum=num_free_transfers)
        self._problem.variables.append(var_transfers)
        self._mappings.num_transfers_var = var_transfers

        var_slack = Variable(next(self.id_gen), "transfer slack", minimum=0)
        self._problem.variables.append(var_slack)
        self._transfer_slack_var = var_slack

        # Transfer in
        eqn = Equation(next(self.id_gen), "transfer in limit", 0, 0)
        eqn.terms.append(Term([var_transfers], -1.0))
        eqn.terms.append(Term([var_slack], -1.0))

        for player, var in self._mappings.player_squad_var.items():
            if player in self._request.current_team:
                continue
            trans_in_var = Variable(next(self.id_gen), f"transfer in {player}", 0, 1, is_discrete=True)
            self._problem.variables.append(trans_in_var)
            trans_in_eqn = Equation(next(self.id_gen), f"transfer {player} tie", 0, 0)
            trans_in_eqn.terms = [Term([var]), Term([trans_in_var], -1.0)]
            self._problem.equations.append(trans_in_eqn)
            eqn.terms.append(Term([trans_in_var]))
        self._problem.equations.append(eqn)

        # Sales
        var_sales = Variable(next(self.id_gen), "sales", 0, is_discrete=False)
        self._problem.variables.append(var_sales)
        eqn = Equation(next(self.id_gen), "Sales", 0, 0)
        eqn.terms.append(Term([var_sales], -1.0))

        total = 0
        for player, var in self._mappings.player_squad_var.items():
            if player in self._request.current_team:
                # Trans out
                sell_var = Variable(next(self.id_gen), f"sell {player}", 0, 1)
                self._problem.variables.append(sell_var)

                sell_eqn = Equation(next(self.id_gen), f"sell {player}", 1.0, 1.0)
                sell_eqn.terms = [Term([sell_var]), Term([var])]
                self._problem.equations.append(sell_eqn)

                self._mappings.player_sell_var[player] = sell_var

                term = Term([sell_var], player.selling_price)
                total += player.selling_price
                eqn.terms.append(term)
        print(f"total possible sales {total}")
        self._problem.equations.append(eqn)
        self._mappings.sales = var_sales

