from dataclasses import dataclass, field
from typing import List, Optional

from lp.problem.problem import Problem
from selection.player import Player, Position
from selection.problem_builder import ProblemMappings


@dataclass
class Results:
    team_players: List[Player] = field(default_factory=list)
    bench_players: List[Player] = field(default_factory=list)
    captain: Optional[Player] = None
    vice_captain: Optional[Player] = None

    def pretty_print(self) -> str:
        s = f"\n           {self.print_position(Position.GK)}        \n" \
              f"\n {self.print_position(Position.DF)} \n" \
              f"\n {self.print_position(Position.MF)} \n" \
              f"\n      {self.print_position(Position.FW)}     \n"

        s += "\n Bench:"
        players = [player for player in self.bench_players]
        players.sort(key=lambda x: x.points)
        for p in players:
            s += f" {p}[{p.points}] "
        return s

    def print_position(self, position):
        s = ""
        players = [player for player in self.team_players if player.position == position]
        for p in players:
            s += f"  {p}[{p.points}]"
            if self.captain == p:
                s += "(C)"
            if self.vice_captain == p:
                s += "(V)"
        return s


class ResultsBuilder:
    @staticmethod
    def build(mappings: ProblemMappings) -> Results:
        assert len([var for var in mappings.player_captain_var.values() if var.value > 0.5])

        captain = next(player for player, var in mappings.player_captain_var.items() if var.value > 0.5)
        team_players = [player for player, var in mappings.player_team_var.items() if var.value > 0.5]
        sorted_players = sorted(team_players, key=lambda x : x.points, reverse=True)
        vice_captain = next(player for player in sorted_players if player != captain)
        sold_players = [player for player, var in mappings.player_sell_var.items() if var.value > 0.5]
        print(f"sold players {sold_players}")
        sold_players = [player.points for player, var in mappings.player_sell_var.items() if var.value > 0.5]
        print(f"sold players {sold_players}")

        for player, purchase in mappings.player_purchase_var.items():
            if purchase.value > 0.001:
                print(f"purchase {player} for {purchase.value}")

        for player, squad in mappings.player_squad_var.items():
            if squad.value > 0.001:
                print(f"squad {player} for {squad.value}")

        return Results(
            team_players=team_players,
            bench_players=[player for player, var in mappings.player_bench_var.items() if var.value > 0.5],
            captain=captain,
            vice_captain=vice_captain
        )
