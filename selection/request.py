import math
from dataclasses import dataclass, field
from typing import List, Dict

from selection.player import Player, Position


@dataclass
class Limit:
    minimum: float = -math.inf
    maximum: float = math.inf


class SelectionConstraints:
    def __init__(self):
        self.squad_players: int = 15
        self.team_players: int = 11
        self.max_players_per_team: int = 3
        self.squad_positions: Dict[str, int] = {Position.GK: 2, Position.DF: 5, Position.MF: 5, Position.FW: 3}
        self.team_positions: Dict[str, Limit] = {Position.GK: Limit(1.0, 1.0),
                                                 Position.DF: Limit(3.0, 5.0),
                                                 Position.MF: Limit(3.0, 5.0),
                                                 Position.FW: Limit(1.0, 3.0)}
        self.transfer_limit: int = 2
        self.transfer_penalty: float = 2.0
        self.bench_objective_coefficient: float = 0.2


@dataclass
class SelectionRequest:
    players: List[Player]
    constraints: SelectionConstraints = SelectionConstraints()
    funds: float = 100
    current_team: List[Player] = field(default_factory=list)
    current_gameweek: int = 0

